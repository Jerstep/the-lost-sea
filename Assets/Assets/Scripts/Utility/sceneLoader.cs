﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class sceneLoader : MonoBehaviour {
    private int currentSceneIndex;
    Scene scene;
    private void Awake()
    {
        scene = SceneManager.GetActiveScene();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Boat" || other.tag == "Player")
        {
            //LoadNextScene();
            if (scene.name == "MainMenu")
            {
                SceneManager.LoadScene("Main");
            }

            if (scene.name == "Main")
            {
                Time.timeScale = 1f;
                SceneManager.LoadScene("Cave");
            }

            if (scene.name == "Cave")
            {
                SceneManager.LoadScene("MainMenu");
            }
        }
    }
}
