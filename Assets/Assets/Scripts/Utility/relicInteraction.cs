﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class relicInteraction : MonoBehaviour {

    public GameObject Stone;

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {
            if(Input.GetKeyDown(KeyCode.E))
            {
                Stone.SetActive(false);
                GetComponent<AudioSource>().Play();
            }
        }
    }
}
