﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class textureScroll : MonoBehaviour {
    
    public float scrollSpeed = 0.03f;
    [HideInInspector] public Renderer rend;

    private float counter;

    void Start()
    {
        rend = GetComponent<Renderer>();
    }

    void Update()
    {
        float offset = Time.time * scrollSpeed;
        rend.material.SetTextureOffset("_MainTex", new Vector2(offset, 0));
    }
}
