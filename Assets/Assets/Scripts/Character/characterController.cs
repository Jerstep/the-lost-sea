﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class characterController : MonoBehaviour
{
    public float walkSpeed = 6f;
    public float runSpeed = 10f;
    public float jumpHeight = 5f;
    public float turnSmoothing = 15f;

    private Vector3 movement;
    [HideInInspector]
    public Rigidbody rb;

    public float rotationSpeed = 6f;
    private float tempRotSpeed;
    public int rotationCounter;
    private bool isGrounded;

    public Transform respawnLocation;

    // List for tags that you can jmp on that could be added in the future. 
    public List<string> CanJumpList;

    // Use this for initialization
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        tempRotSpeed = rotationSpeed;        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float lh = Input.GetAxisRaw("Horizontal");
        float lv = Input.GetAxisRaw("Vertical");

        Move(lh, lv);

        if (Input.GetKey(KeyCode.Space))
        {
            if(isGrounded == true)
            {
                rb.AddForce(new Vector3(0, jumpHeight, 0), ForceMode.Impulse);
                isGrounded = false;
            }
        }
    }

    void Move(float lh, float lv)
    {
        movement.Set(lh, 0f, lv);
        movement = Camera.main.transform.TransformDirection(movement);
        movement.y = 0f;

        if (Input.GetKey(KeyCode.LeftShift))
        {
            movement = movement.normalized * runSpeed * Time.deltaTime;
            rotationSpeed *= 0.9f;
        }
        else
        {
            movement = movement.normalized * walkSpeed * Time.deltaTime;
            rotationSpeed = tempRotSpeed;
        }

        rb.MovePosition(rb.position + movement);


        if (lh != 0f || lv != 0f)
        {
            Rotating(lh, lv);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "respawn")
        {
            transform.position = respawnLocation.position;
        }
    }

    void OnCollisionEnter(Collision other)
    {
        print("colission");
        for(int i =0; i < CanJumpList.Capacity; i++)
        {
            if (other.gameObject.tag == CanJumpList[i])
            {
                isGrounded = true;
            }
            print(isGrounded);
        }
    }

    void Rotating(float lh, float lv)
    {
        Vector3 targetDirection = new Vector3(lh, 0f, lv);
        Quaternion targetRotation = Quaternion.LookRotation(movement, rb.transform.up);
        Quaternion newRotation = Quaternion.Lerp(rb.transform.rotation, targetRotation, turnSmoothing * Time.deltaTime);
        rb.MoveRotation(newRotation);
    }
}


